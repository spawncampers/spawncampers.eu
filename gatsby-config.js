module.exports = {
  siteMetadata: {
    title: 'SC clan of Xonotic (a. k. a. spawncampers)',
    description: 'Homepage of famous Xonotic SC clan',
    author: 'breakfast',
  },
  plugins: [
    'gatsby-plugin-styled-components',
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/articles`,
        name: `markdown-pages`,
      },
    },
    `gatsby-transformer-remark`,
  ],
};
