import React from 'react';
import { Link, graphql } from 'gatsby';
import Layout from '../components/layout';
import SEO from '../components/seo';
import Logo from '../components/Logo';
import Footer from '../components/Footer';

import styled, { ThemeProvider } from 'styled-components';
const Page = styled.div`
  padding: 5em 1em;
  box-sizing: border-box;
  width: 380px;
  margin: 0 auto;
`;
const Section = styled.section`
  margin-bottom: 2em;
`;
const Intro = styled.div`
  text-align: ${props => props.align};
  margin-bottom: 0.8em;
  font-family: 'Xolonium';
  color: ${({ theme: { color } }) => color};
  white-space: nowrap;
`;
const Heading = styled.h1`
  text-align: center;
  color: ${({ theme: { color } }) => color};
  margin-top: 3em;
  margin-bottom: 1em;
  font-family: 'Xolonium';
  font-size: inherit;
`;
const Member = styled.div`
  color: ${({ theme: { color } }) => color};
  text-align: center;
  margin-bottom: 0.1em;
`;
const Allies = styled.div`
  display: inline-block;
  margin-bottom: 0.1em;
  display: flex;
`;
const Ally = styled.div`
  flex: 1;
  color: ${({ theme: { color } }) => color};
  text-align: center;
`;
const Server = styled.div`
  color: ${({ theme: { color } }) => color};
  white-space: nowrap;
`;
const Article = styled.div`
  color: ${({ theme: { color } }) => color};
  white-space: nowrap;
`;

export default ({ data }) => (
  <Layout>
    <SEO title="Home" />
    <Page>
      <ThemeProvider theme={{ color: '#00ff0f' }}>
        <Section>
          <Intro align="left">some people laughed at us</Intro>
          <Intro align="right">
            some people didn't believe
            <br />
            that this could be true
          </Intro>
          <Intro align="center">but reality is</Intro>
          <Intro align="center">we are</Intro>
        </Section>
      </ThemeProvider>

      <Logo />

      <ThemeProvider theme={{ color: '#02fbfe' }}>
        <Section>
          <Heading>-:¦:- members -:¦:-</Heading>
          {[
            {
              url: 'http://stats.xonotic.org/player/130213',
              name: 'breakfast',
            },
            { url: 'http://stats.xonotic.org/player/2403', name: '[力] Jeff' },
            { url: 'http://stats.xonotic.org/player/30489', name: 'Yurashina' },
            { url: 'http://stats.xonotic.org/player/89685', name: 'LONGACAT' },
          ].map(member => (
            <Member>
              ‣ <a href={member.url}>{member.name}</a>
            </Member>
          ))}
        </Section>
      </ThemeProvider>

      <ThemeProvider theme={{ color: '#1fcaf8' }}>
        <Section>
          <Heading>-:¦:- allies -:¦:-</Heading>
          {[
            '∫ıгRaŋjı∂',
            'Carl',
            'FireBreaker',
            'gnom',
            'Jay',
            'JEKKI',
            'Julius',
            'Morphed',
            'packer',
            'wurstkoffer',
            'мµѕιcgoαт',
          ]
            .sort((a, b) => a.localeCompare(b))
            .reduce((result, value, index, array) => {
              if (index % 3 === 0) result.push(array.slice(index, index + 3));
              return result;
            }, [])
            .map(items => (
              <Allies>
                {items.map(item => (
                  <Ally>{item}</Ally>
                ))}
              </Allies>
            ))}
        </Section>
      </ThemeProvider>

      <ThemeProvider theme={{ color: '#4c7fef' }}>
        <Section>
          <Heading>-:¦:- servers -:¦:-</Heading>
          <Server>
            ‣ Vehicle ....... <a href="http://jeffs.eu/vehicle.html">www</a> ..{' '}
            <a href="http://stats.xonotic.org/server/11019">stats</a> .. jeffs.eu:26010
          </Server>
          <Server>
            ‣ Resurrection .. <a href="http://jeffs.eu/resurrection.html">www</a> ..{' '}
            <a href="http://stats.xonotic.org/server/21010">stats</a> .. jeffs.eu:26015
          </Server>
          <Server>
            ‣ Nostalgia ............ <a href="http://stats.xonotic.org/server/13735">stats</a> .. jeffs.eu:26011
          </Server>
        </Section>
      </ThemeProvider>

      <ThemeProvider theme={{ color: '#7091E5' }}>
        <Section>
          <Heading>-:¦:- articles -:¦:-</Heading>
          {data.allMarkdownRemark.edges.map(edge => (
            <Article>
              <Link to={edge.node.frontmatter.path}>‣ {edge.node.frontmatter.title}</Link>
            </Article>
          ))}
        </Section>
      </ThemeProvider>
    </Page>
    <Footer />
  </Layout>
);

export const pageQuery = graphql`
  {
    allMarkdownRemark(limit: 1000) {
      edges {
        node {
          frontmatter {
            path
            title
          }
        }
      }
    }
  }
`;
