import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import Footer from '../components/Footer';
import styled from 'styled-components';

const Page = styled.div`
  padding: 5em 1em;
  box-sizing: border-box;
  width: 380px;
  margin: 0 auto;
`;

export default () => (
  <Layout>
    <SEO title="404: Not found" />
    <Page>
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Page>
    <Footer />
  </Layout>
);
