---
path: '/vehicles-modpack'
title: 'Vehicles in Jeffs ModPack'
---
# This information is old, maybe there are things that are no longer stable.

# About the unofficial vehicles of this mod

## General information

R22 Helicopter and LL48 Tank were used in the SMB modpack and were removed because these are no longer considered part of the core mod. Perhaps these were removed in October 2018.

# Unofficial vehicles

## Light APC

![Light APC outside](/img/Vehicle-lightapc.png)

![Light APC cockpit](/img/Vehicle-lightapc-cockpit.png)

Up to three players can ride the Light APC, a large tank vehicle, at the same time.
The first player will be the pilot, who can drive. It can keep more players inside too.

![Light APC third person view](/img/Vehicle-lightapc-thirdperson-view.png)

The second player will be the front gunner. The third player will be the back gunner.
When the pilot leaves Light APC, the second player becomes the pilot.
There are no secondary weapons for any of the three riders in Light APC.

## R22 Helicopter

![R22 Helicopter outside](/img/Vehicle-r22helicopter.png)

![R22 Helicopter cockpit](/img/Vehicle-r22helicopter-cockpit.png)

The R22 Helicopter takes one pilot who can also operate two weapons. There are two different reticles for them. Only two players can ride the R22 Helicopter. The second player will be the cockpit, but this player won't do nothing, only will be the spectator of the vehicle inside and he can get out of the vehicle whenever he wants, the same for the pilot.

![R22 Helicopter third person view](/img/Vehicle-r22helicopter-thirdperson-view.png)

The primary weapon is machine gun and is shot toward the green reticle. The second weapon is remote-controlled missiles that target any nearby object that is around the front, is dropped onto the ground the green reticle is pointing at. The white reticle always points at the head of the vehicle. The green reticle is projected by the movement momentum of the vehicle. The movement controls are the same as [Raptor vehicle](https://gitlab.com/xonotic/xonotic/-/wikis/Vehicles#raptor).

## LL48 Tank

![LL48 Tank outside](/img/Vehicle-ll48tank.png)

![LL48 Tank cockpit](/img/Vehicle-ll48tank-cockpit.png)

LL48 Tank takes one rider. It walks on the ground and can jump from very high altitude while protecting the rider.

![LL48 Tank third person view](/img/Vehicle-ll48tank-thirdperson-view.png)

Its primary weapon is the HLAC; the aiming direction is shown by the orange reticle, which follows the front of the vehicle, it does the same as Light APC front gunner. The secondary weapon is the Tank Cannonball that are aimed with the red reticle, which always points to the front of the vehicle, reloading ammo takes a bit of time. Press SHIFT to move the secondary weapon down and press SPACE to move up.

# Notes for mappers

Vehicle class names for mappers:

- Light APC: "vehicle_lightapc"
- R22 Helicopter: "vehicle_r22heli"
- LL48 Tank: "vehicle_tankll48"

# Notes for developers

Official vehicles codes are in this [directory](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/master/qcsrc/common/vehicles/vehicle).
There are unofficial vehicles codes stored in the [SMB modpack](https://github.com/MarioSMB/modpack/tree/master/mod/common/vehicles).
Some unofficial vehicles are in the jeff-modpack.

Maps needed, `as` and `ctf` for now - I can't take this much further w/o play testing on a larger scale.

# More info

Official vehicles are in [this link](https://gitlab.com/xonotic/xonotic/-/wikis/Vehicles).
