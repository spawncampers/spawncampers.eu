---
path: '/recommended-xonotic-settings'
title: 'Recommended Xonotic settings'
---

## Input

### Mouse sensitivity

Make sure that your mouse has plenty of space to move (at least 15cm). Reduce your sensitivity to amount that will allow you to comfortable do a full 360° turn.

Here are some essential keyboard shortcuts:

- `F3` — spectate;
- `F5` — change team;
- `F11` — disconnect from server;
- `F10` — quit game;
- `b` — open in-game quick menu;
- `e` — use item (hook or grenade, depending on server);
- `t` — chat;
- `y` — team chat;
- `f` — drop weapon or ride a player/monster.

## Configuration files

First you need to find your Xonotic data directory:

| Operating System | Data directory Location                           |
| ---------------- | ------------------------------------------------- |
| Windows          | `C:\\Users\\user\\Saved Games\\xonotic\\data`     |
| GNU+linux        | `~/.xonotic/data`                                 |
| macOS            | `Home → Library/Application Support/xonotic/data` |

Data directory may contain one of following config files:

- `config.cfg` — generated automatically by the game;
- `autoexec.cfg` — user config that will be automatically executed on game start.

## Visuals

Place these settings in your `autoexec.cfg`:

Essential settings:

```
hud_damage_blur 0 // do not blur the screen when damage is taken
r_bloom 0 // disable bloom effect that washes out the image
```

Optional player visibility improvements. This will disable custom models and will make players display at maximum vibrance:

```
playermodel "models/player/megaerebus.iqm" // use player model with full brightness
cl_forceplayermodels 1 // other players will be displayed using your models
cl_forceplayercolors 1 // other players will be displayed using your colours
```
