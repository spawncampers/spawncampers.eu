import React from 'react';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Xolonium';
    font-weight: normal;
    font-style: normal;
    font-display: swap;
    src: url('/xolonium.woff2') format('woff2');
  }

  @font-face {
    font-family: 'Ubuntu Mono';
    font-style: normal;
    font-weight: normal;
    font-display: swap;
    src: url('/ubuntu-mono.woff2') format('woff2');
  }

  html {
    background: black;
  }

  body {
    margin: 0;
    font-size: 10pt;
    font-family: 'Ubuntu Mono', monospace;
    color: white;
  }

  a {
    color: inherit;
    text-decoration: none;

    &:hover, &:focus {
      text-decoration: underline;
    }
  }

  // * {
  //   outline: 1px solid yellow;
  // }
`;

export default ({ children }) => (
  <>
    <GlobalStyle />
    {children}
  </>
);
