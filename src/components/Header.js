import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';

const Header = styled.header`
  padding: 1em;
  display: flex;
`;

const GoHome = styled(Link)`
  flex: 100px 0 0;
`;

const Space = styled.div`
  flex: 1;
`;

const Download = styled.a`
  flex: none;
  padding: 0.5em;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Img = styled.img`
  width: 100%;
`;

export default () => (
  <Header>
    <GoHome to="/">
      <Img src="/logo.jpg" />
    </GoHome>
    <Space />
    <Download target="_blank" href="https://xonotic.org/">
      Download Xonotic
    </Download>
  </Header>
);
