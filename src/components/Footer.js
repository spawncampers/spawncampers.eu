import React from 'react';
import styled from 'styled-components';

const Footer = styled.div`
  text-align: center;
  padding: 1em;
  color: #444;
`;

export default () => (
  <Footer>
    © {new Date().getFullYear()}, spawncampers |{' '}
    <a href="https://gitlab.com/sc-breakfast/spawncampers.eu">source code</a>
  </Footer>
);
