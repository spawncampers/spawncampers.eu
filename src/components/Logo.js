import React from 'react';
import styled, { keyframes } from 'styled-components';

const Wrapper = styled.div`
  perspective: 500px;
  ${({ small }) =>
    small &&
    `
    font-size: 4pt;
  `}
  display: flex;
  justify-content: center;
`;

const Logo = styled.div`
  display: inline-block;
  background: linear-gradient(
    to right,
    rgb(0, 255, 0),
    rgb(0, 255, 160),
    rgb(0, 200, 255),
    rgb(0, 255, 160),
    rgb(0, 255, 0)
  );
  background-clip: text;
  -webkit-background-clip: text;
  color: transparent;
  animation: ${keyframes`
    from {
      background-position-x: 0px;
      text-shadow:
        -4px 0 8px rgb(0, 255, 0),
         4px 0 8px rgb(0, 255, 0);
    }
    50% {
      text-shadow:
        -4px 0 8px rgb(0, 200, 255),
         4px 0 8px rgb(0, 200, 255);
    }
    to {
      background-position-x: 500px;
      text-shadow:
        -4px 0 8px rgb(0, 255, 0),
         4px 0 8px rgb(0, 255, 0);
    }
  `} ${({ speed }) => speed} linear infinite,
    ${keyframes`
    from {
      transform: rotate3d(0, 1, 0, 7deg);
    }
    to {
      transform: rotate3d(0, 1, 0, -7deg);
    }
  `} ${({ speed }) => speed} alternate ease-in-out infinite;
  background-size: 500px 100%;
  white-space: pre;
  font-weight: bold;
  text-align: left;
`;

// $ figlet cricket SC`clan
export default ({ calm = false, small = false }) => (
  <Wrapper small={small}>
    <Logo speed={calm ? '20s' : '2s'}>{`
 _______ _______   __
|   _   |   _   : |  |
|   |___|.  |___|  |_|   .--.
|____   |.  |___    .----|  :---.-.-----.
|:  |   |:  |   |   |  __|  |  _  |     |
|::.. . |::.. . |   |____|__|___._|__|__|
\`-------\`-------'
    `}</Logo>
  </Wrapper>
);
