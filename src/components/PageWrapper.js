import React from 'react';
import styled from 'styled-components';

const PageWrapper = styled.main`
  max-width: 800px;
  margin: 0 auto;
  padding: 2em 1em;
  box-sizing: border-box;
`;

export default ({ children }) => <PageWrapper>{children}</PageWrapper>;
