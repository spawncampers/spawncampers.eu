import React from 'react';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';

export default ({ title }) => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `,
  );

  return (
    <Helmet
      htmlAttributes={{
        lang: 'en',
        dir: 'ltr',
        prefix: 'og: http://ogp.me/ns#',
      }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        { name: `description`, content: site.siteMetadata.description },
        { property: `og:title`, content: title },
        { property: `og:description`, content: site.siteMetadata.description },
        { property: `og:type`, content: `website` },
        { property: `og:image`, content: `banner.jpg` },
        { name: `twitter:title`, content: title },
        { name: `twitter:description`, content: site.siteMetadata.description },
        { name: `twitter:card`, content: `summary` },
        { property: `twitter:image`, content: `banner.jpg` },
        { name: `twitter:creator`, content: site.siteMetadata.author },
      ]}
    />
  );
};
