import { graphql } from 'gatsby';
import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/layout';
import Header from '../components/Header';
import SEO from '../components/seo';
import PageWrapper from '../components/PageWrapper';

export default ({
  data: {
    markdownRemark: { frontmatter, html },
  },
}) => (
  <Layout>
    <PageWrapper>
      <Header />
      <SEO title={frontmatter.title} />
      <h1>{frontmatter.title}</h1>
      <div dangerouslySetInnerHTML={{ __html: html }} />
      <Link to="/">Go back to the homepage</Link>
    </PageWrapper>
  </Layout>
);

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        path
        title
      }
    }
  }
`;
